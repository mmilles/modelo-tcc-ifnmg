# MODELO DE TCC IFNMG

Este é meu Trabalho de Conclusão de Curso apresentado ao Instituto Federal de Educação, Ciência e Tecnologia do Norte de Minas Gerais (IFNMG) – Campus Januária como parte das exigências do Programa de Graduação em Licenciatura em Matemática, para obtenção do título de Licenciado em Matemática. Seu título é ESTUDO DOS ALGORITMOS MULTIPLICATIVOS ADOTADOS PELAS CIVILIZAÇÕES ANTIGAS, sob a orientação do Prof. Gustavo Pereira Gomes.

Para além do TCC, este trabalho contém uma classe de formatação de Monografia conforme as Normas do IFNMG, que serve como modelo para outros utilizarem. Para fazer esta classe contei com a ajuda do meu orientador Gustavo que me apresentou o LaTeX e do colega Ítalo Antônio que foi companheiro de estudo. Desde já deixo meus agradecimentos.


Qualquer dúvida ou sugestão entre em contato com mmarcos.miller@gmail.com.


