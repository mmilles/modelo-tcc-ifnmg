% **
% Resultados e discussões.
% *
\section{OS ALGORITMOS}
	\subsection{Egípcio} \label{algoritmo-egipcio} \index{Algoritmo egípcio}
		Suponha que queremos multiplicar dois números inteiros positivos $a$ e $b$. O método egípcio funciona da seguinte forma:
		\begin{enumerate}[1º]
			\item Escolha um dos fatores a serem multiplicados, suponha que seja $a$.
			\item Construa uma tabela com duas colunas, na primeira coloque o número 1, e na segunda escreva o fator escolhido na 1ª etapa.
			\item Faça duplicações dos números em ambas as colunas até que, na primeira coluna, o último número duplicado seja igual ou menor que o fator $b$.
			\item Na primeira coluna, escolha os valores que somados deem o fator $b$ escolhido.
			\item O resultado da multiplicação de $a$ por $b$ é obtido somando os valores da segunda coluna, correspondentes aos valores escolhidos na 4ª etapa.
		\end{enumerate}
		
		\begin{exemplo}\label{ex1-egipcio} Vamos multiplicar os números 13 e 37. Seguindo os passos do algoritmo:	\\
			Primeiramente escolheremos o número 13.
			
			\noindent Agora construímos o esboço da tabela,
			\begin{center}\begin{tabular}{c|c} \hline 1 & 13 \\ \hline \end{tabular}\end{center}
			
			\noindent Em seguida fazemos as duplicações em ambas as colunas, e obtemos
			\begin{center}\begin{tabular}{c|c}
					\hline
					1	&	13 	\\ \hline
					2	&	26 	\\ \hline
					4	&	52 	\\ \hline
					8	&	104 \\ \hline
					16	&	208 \\ \hline
					32	& 	416 \\ \hline
			\end{tabular}\end{center}
			
			\noindent Selecionando os números da primeira coluna que somados deem 37,
			\begin{center}\begin{tabular}{c c|c}
					\cline{2-3}	$\to$	& 1 	& 13 \\
					\cline{2-3}			& 2 	& 26 \\
					\cline{2-3}	$\to$	& 4 	& 52 \\
					\cline{2-3}			& 8 	& 104 \\
					\cline{2-3}			& 16	& 208 \\
					\cline{2-3}	$\to$	& 32	& 416 \\
					\cline{2-3}
			\end{tabular}\end{center}
			
			\noindent E por último, a soma dos números da segunda coluna, correspondente aos números escolhidos na quarta etapa, é o produto de 13 por 37, ou seja, $$13 \cdot 37 = 13 + 52 + 416 = 481.$$
		\end{exemplo}
		
		\begin{exemplo} Do mesmo modo do exemplo anterior, vamos multiplicar 32 e 27.	\\
			\noindent Primeiro escolhemos o número 27.
			
			\noindent Construímos a tabela,
			\begin{center}\begin{tabular}{c|c} \hline 1 & 27 \\ \hline \end{tabular}\end{center}
			
			\noindent Fazendo as duplicações em ambas as colunas, e selecionando os números na primeira coluna que somados dêem 32, obtemos		
			\begin{center}\begin{tabular}{c c|c}
					\cline{2-3}			& 1 & 27	\\
					\cline{2-3} 		& 2 & 54	\\
					\cline{2-3} 		& 4 & 108	\\
					\cline{2-3} 		& 8 & 216	\\
					\cline{2-3} 		& 16 & 432	\\
					\cline{2-3}	$\to$	& 32 & 864	\\
					\cline{2-3}
			\end{tabular}\end{center}
			
			\noindent Finalizamos fazendo soma dos números da segunda coluna, correspondente aos valores escolhidos na quarta etapa, o resultado da soma será o produto de 27 por 32, $$27 \cdot 32 = 864.$$
		\end{exemplo}
		
		\begin{observacao}
			Se um dos números a serem multiplicados é uma potência de 2, os cálculos ficam mais simples escolhendo esse número como sendo o fator $b$. Por exemplo, vamos multiplicar 256 por 67. Como 256 é uma potência de base dois, $256=2^8$, iremos escolher $b=256$. Assim, obtemos a tabela
			\begin{center}\begin{tabular}{c c|c}
					\cline{2-3}			& 1		& 67		\\
					\cline{2-3}			& 2 	& 134		\\
					\cline{2-3}			& 4		& 268		\\
					\cline{2-3}			& 8		& 536		\\
					\cline{2-3}			& 16	& 1072		\\
					\cline{2-3}			& 32	& 2144		\\
					\cline{2-3}			& 64	& 4288		\\
					\cline{2-3}			& 128	& 8576		\\
					\cline{2-3}	$\to$ 	& 256	& 17152		\\
					\cline{2-3}
			\end{tabular}\end{center}
		
			e, portanto, $256\cdot 67=17152$.
		\end{observacao}
		
		\subsubsection{Explicando o Algoritmo Egípcio}
			A validade do método egípcio é justificada pelo Teorema \ref{teoremaNQB}, em que podemos escrever um número em qualquer base e na propriedade distributiva da multiplicação em relação a adição. Sendo assim, sejam $a$ e $b$ dois números inteiros positivos representados na base 10. Pelo Teorema \ref{teoremaNQB}, podemos escrever $b$ na base 2, ficando da seguinte forma
			\begin{align} b=2^{k_1}+2^{k_2}+\cdots+2^{k_m},\,k_i\in\N\textrm{ para todo }1\leq i\leq m,\end{align}
			\noindent onde $k_1<k_2<\cdots<k_m$. Aplicando os procedimentos do algoritmo egípcio, temos inicialmente
			
			\begin{center}\begin{tabular}{c c|c}
					\cline{2-3}		& $1$			& $a$				\\
					\cline{2-3}		& $2^1$ 		& $2\cdot a$		\\
					& $2^2$ 		& $2^2\cdot a$						\\
					& $\vdots$ 		& $\vdots$							\\
					$\to$			& $2^{k_1}$ 	& $2^{k_1}\cdot a$	\\
					& $\vdots$ 		& $\vdots$							\\
					$\to$			& $2^{k_2}$ 	& $2^{k_2}\cdot a$	\\
					& $\vdots$ 		& $\vdots$							\\
					$\to$			& $2^{k_m}$ 	& $2^{k_m}\cdot a$	\\ \cline{2-3}
			\end{tabular}\end{center}
		
			Como resultado, temos a expressão $2^{k_1}\cdot a+2^{k_2}\cdot a+\cdots+2^{k_m}\cdot a$. Como $a$ é fator comum, então colocamo-os em evidência obtendo, de fato a multiplicação de $a$ por $b$
			\begin{align} a\cdot(2^{k_1}+2^{k_2}+\cdots+2^{k_m})=a\cdot b. \end{align}
			
			
	\subsection{Hindu}
		O processo de multiplicação hindu é bem parecido com o que utilizamos hoje. Os árabes se apropriaram deste método, e mais tarde o levaram para a Europa Ocidental, onde sofreu alterações até chegar no que é hoje. Os matemáticos hindu’s faziam os cálculos utilizando tábuas coberta de areia ou farinha, ou lousas com tinta branca removível, tudo isso para facilitar as correções. Diferentes dos árabes que adaptaram o processo hindu para ser feito em papel. Vários métodos eram utilizados, mas o mais conhecido é o da multiplicação Gelosiana, ou multiplicação em reticulado, ou em grade.
	
		Suponha que queremos multiplicar dois números inteiros positivos $a$ e $b$. O método hindu funciona da seguinte maneira:
		\begin{enumerate}[1º]
			\item Crie uma tabela onde o número de algarismos de $a$ e $b$ são, respectivamente, o número de colunas e de linhas da tabela. Os algarismos de $a$ ficam na parte superior da tabela e os de $b$ ficam à esquerda, colocando-os de baixo para cima da tabela.
			\item Trace uma diagonal, em cada célula, começando de cima para baixo e da esquerda para a direita.
			\item Multiplique o algarismo da primeira linha com o da primeira coluna e coloque o resultado na respectiva célula, em que o algarismo da dezena fica a baixo da diagonal e a unidade acima. Repita o procedimento em todas as colunas e faça o mesmo procedimento para a próxima linha.
			\item Some os dígitos das fileiras diagonais, iniciando do lado direito da tabela, caso a soma dê um número maior que nove, escreva o algarismo da dezena na próxima diagonal e repita o processo até a última diagonal.
			\item O número formado pelos dígitos (encontrados na 4ª etapa) abaixo e à direita da tabela, nessa ordem, será o resultado da multiplicação.
		\end{enumerate}
		
		\begin{exemplo}\label{ex1-hindu} Vamos multiplicar 13 por 24. De acordo com o método, temos primeiramente
			\begin{center}\begin{tabular}{c|c|c|c}
					\multicolumn{1}{c}{} & \multicolumn{1}{c}{\textbf{1}} & \multicolumn{1}{c}{\textbf{3}} & \multicolumn{1}{c}{} \\
					\cline{2-3}
					\textbf{4} & "" & "" & "" \\
					\cline{2-3}
					\textbf{2} & "" & "" & "" \\
					\cline{2-3}
					\multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{} \\
			\end{tabular}\end{center}
			
			\noindent Em seguida, efetuando segundo e terceiro procedimentos, obtemos
			\begin{center}\begin{tabular}{c|c|c|c}
					\multicolumn{1}{c}{} & \multicolumn{1}{c}{\textbf{1}} & \multicolumn{1}{c}{\textbf{3}}	\\
					\cline{2-3}
					\textbf{4} & \diagbox{0}{4} & \diagbox{1}{2}	\\
					\cline{2-3}
					\textbf{2} & \diagbox{0}{2} & \diagbox{0}{6}	\\
					\cline{2-3}
					\multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{}	\\
			\end{tabular}\end{center}
			
			\noindent E pela quarta e quinta etapas, obtemos
			\begin{center}\begin{tabular}{c c c}
					\begin{minipage}{.3\textwidth} \centering \includegraphics[scale=0.25]{imagens/hindu/ex1_sem-setas} \end{minipage} &
					$\longrightarrow$ &
					\begin{minipage}{.3\textwidth} \centering \includegraphics[scale=0.223]{imagens/hindu/ex1_com-setas} \end{minipage}
			\end{tabular}\end{center}
			
			\noindent Portanto, pelo algoritmo hindu, $13 \cdot 24 = 312$.
		\end{exemplo}
	
		\begin{exemplo} Iremos agora fazer o produto de 17 por 237. Seguindo o algoritmo hindu, temos inicialmente  pelas primeira, segunda e terceira etapas a seguinte tabela
			
			\begin{center} \includegraphics[scale=0.27]{imagens/hindu/ex2} \end{center}
			
			\noindent Agora, realizando os precedimentos descritos pelas quarta e quinta etapas, temos
			\begin{center}\begin{tabular}{ c c c }
					\begin{minipage} {.3\textwidth} \centering \includegraphics[scale=0.189]{imagens/hindu/ex2_sem-setas} \end{minipage} &
					$\longrightarrow$ &
					\begin{minipage} {.3\textwidth} \centering \includegraphics[scale=0.189]{imagens/hindu/ex2_com-setas} \end{minipage}
			\end{tabular}\end{center}
			
			\noindent Assim, temos que o resultado da multiplicação $17 \cdot 237$ é 4029.
		\end{exemplo}
		
		\begin{exemplo} A Multiplicação de 732 por 138, realizando o procedimento hindu, é igual à 101.016, conforme pode ser visto nas tabelas abaixo:
			
			\begin{center}\begin{tabular}{ c c c }
					\begin{minipage}{.3\textwidth} \centering \includegraphics[scale=0.18]{imagens/hindu/ex3_sem-setas} \end{minipage} &
					$\longrightarrow$ &
					\begin{minipage}{.3\textwidth} \centering \includegraphics[scale=0.18]{imagens/hindu/ex3_com-setas} \end{minipage}
			\end{tabular}\end{center}
		\end{exemplo}
		
		\subsubsection{Explicando o Algoritmo Hindu} \label{explicao-hindu}
			Para explicar o método hindu, vamos utilizar números inteiros positivos com dois algarismos devido a complexidade da demostração. Mas o método é válido para números com quaisquer quantidades de algarismos.
			
			Considere dois números inteiros positivos $x$ e $y$, no qual podemos representá-los, de acordo com o Teorema \ref{teoremaNQB}, na base 10 da forma $x = m_1 10 + m_0$ e $y = n_1 10 + n_0$. Efetuando a multiplicação de $x$ por $y$ pelo método hindu, obtemos
			
			\begin{center}\begin{tabular}{c|c|c|}
					\multicolumn{1}{c}{} & \multicolumn{1}{c}{\textbf{$m_1$}} & \multicolumn{1}{c}{\textbf{$m_0$}}	\\
					\cline{2-3}
					\textbf{$n_0$} & \diagbox{$a$}{$b$} & \diagbox{$c$}{$d$}	\\
					\cline{2-3}
					\textbf{$n_1$} & \diagbox{$e$}{$f$} & \diagbox{$g$}{$h$}	\\
					\cline{2-3}
					\multicolumn{1}{c}{} & \multicolumn{1}{c}{} & \multicolumn{1}{c}{}	\\
			\end{tabular}\end{center}
			\vspace*{-0.5cm}
			Desse modo, o resultado da multiplicação é o número escrito na forma
			\begin{gather} e10^3 + (a+f+g)10^2 + (b+c+h)10 + d \label{equacao-hindu} \end{gather} 
			e, ainda pela tabela acima, verifica-se as seguintes relações:
			\begin{align*}
				n_0m_1 &= (ab)_{10} = a10 + b	\\
				n_0m_0 &= (cd)_{10} = c10 + d	\\
				n_1m_1 &= (ef)_{10} = e10 + f	\\
				n_1m_0 &= (gh)_{10} = g10 + h
			\end{align*}
			Sendo assim, desenvolvendo a expressão (\ref{equacao-hindu}) com o auxílio das expressões acima, temos
			\begin{gather}
				e10\cdot 10^2 + a10^2 + f10^2 + g10^2 + b10 + c10 + h10 +d	\nonumber	\\
				(a10 + b)10 + (c10 + d) + (e10 + f)10^2 + (g10 + h)10		\nonumber	\\
				n_0m_1 10 + n_0m_0 + n_1m_1 10^2 + n_1m_0 10				\nonumber	\\
				n_1m_1 10^2 + (n_0m_1 + n_1m_0)10 + n_0m_0					\nonumber	\\
				m_1n_1 10^2 + (m_1n_0 + m_0n_1)10 + m_0n_0					\label{exp4}
			\end{gather}
			
			Note que a expressão (\ref{exp4}) é a mesma encontrada quando efetua-se o método multiplicativo atual utilizando a distributividade da multiplicação em relação a adição, mostrando assim, a validade do método hindu.
		
		
	\subsection{Chinês} \label{algoritmo-chines}
		Os chineses utilizavam varetas de bambus para efetuar suas multiplicações e esse processo é uma variante do processo hindu, mas que não é originário deste. As varetas ficavam dispostas na horizontal e na vertical. O método multiplicativo chinês funciona da seguinte forma, para números inteiros positivos $a$ e $b$, segue-se os passos
		
		\begin{enumerate}[1º]
			\item Com os algarismos que formam o fator $a$, represente-os sua quantidade com grupos de varetas na posição vertical da esquerda para a direita deixando um espaço entre os grupos, onde cada grupo de vareta representa o número de cada casa decimal de $a$.
			
			\item Com os algarismos do fator $b$, crie grupos de varetas horizontais que represente-os sobre as varetas do fator $a$, deixando um espaço entre os grupos.
			
			\item Some os pontos de interseções das varetas, em forma de diagonal, iniciando do lado direito, e caso a soma dê um número maior que nove, acrescente o algarismo das dezenas à soma da próxima diagonal e repita o processo até a última diagonal.
			
			\item Os números formados pelos dígitos encontrados no item 3, da esquerda e abaixo, nessa ordem, é o resultado da multiplicação de $a$ por $b$.
		\end{enumerate}
		
		\begin{exemplo}\label{ex1-chines}
			Utilizando o algoritmo chinês, vamos a multiplicação  $13\cdot24$. Executando o primeiro e segundo passo, assim organizamos os grupos de varetas na vertical dos algarismos do número 13, fazendo o mesmo na horizontal com os algarismos do número 24,
			\begin{center} \includegraphics[scale=0.3]{imagens/chinesa/ex1/ex1_A} \end{center}
			
			\noindent E de acordo com o terceiro item, somamos as interseções dos segmentos,
			\begin{center} \includegraphics[scale=0.3]{imagens/chinesa/ex1/ex1_B} \end{center}
			
			\noindent Finalmente no quarto item, o resultado da multiplicação de 13 por 24 é 312. Como mostra a figura abaixo,
			\begin{center} \includegraphics[scale=0.3]{imagens/chinesa/ex1/ex1_C} \end{center}
		\end{exemplo}
		
		\begin{exemplo}
			Vamos multiplicar dois números, 145 por 31. Seguindo os passos do algoritmo, obtemos o seguinte resultado,
			\begin{center}\begin{tabular}{c c c}
					\begin{minipage}{.4\textwidth} \centering \includegraphics[scale=0.3]{imagens/chinesa/ex2/ex2_B} \end{minipage} &
					$\longrightarrow$ &
					\begin{minipage}{.4\textwidth} \centering \includegraphics[scale=0.3]{imagens/chinesa/ex2/ex2_C} \end{minipage}
			\end{tabular}\end{center}
			\noindent Deste modo, o resultado da multiplicação  é $145\cdot31 = 4495$.
		\end{exemplo}
		
		\subsubsection{Explicando o Algoritmo Chinês}
		Note que comparando os exemplos \ref{ex1-chines} e \ref{ex1-hindu}, percebemos que existe uma grande similaridade entre os métodos chinês e hindu, pois os cálculos são os mesmos. Portanto, sua explicação matemática é a mesma abordada no capítulo \ref{explicao-hindu}.
		
		
	\subsection{Russo}\label{algoritmo-russo}
		Suponha que queremos multiplicar dois números inteiros positivos $a$ e $b$. O método desenvolvido pelos camponeses russos é uma variação do procedimento egípcio e funciona da seguinte forma:
		\begin{enumerate}[1º]
			\item Construa uma tabela com duas colunas, colocando os números a serem multiplicados na primeira linha, um em cada coluna.
			\item Faça duplicações para no valor da coluna da direita e, paralelamente, divida pela metade os valores da coluna da esquerda, porém, quando a divisão não for exata, desconsidere a parte fracionária.
			\item Repita o procedimento acima até encontrar o número um como resultado na primeira coluna.
			\item Após o 3º passo, desconsidere as linhas cujo número da coluna da esquerda for par.
			\item O resultado da multiplicação será a soma dos números da coluna da direita que restaram depois da eliminação realizada no 4º passo.
		\end{enumerate}
		
		\begin{exemplo}
			Vamos multiplicar os números 43 e 51. Aplicando o método russo, primeiro construímos a tabela
			\begin{center}\begin{tabular}{c|c} \hline 43 & 51 \\ \hline \end{tabular}\end{center}
			
			\noindent fazendo as duplicações na coluna da direita e divisões por dois na coluna da esquerda, obtemos
			\begin{center}\begin{tabular}{c|c}
					\hline
					43	& 51		\\ \hline
					21	& 102		\\ \hline
					10	& 204		\\ \hline
					5	& 408		\\ \hline
					2	& 816		\\ \hline
					1	& 1632		\\ \hline
			\end{tabular}\end{center}
			
			\noindent Agora desconsiderando as linhas cujo número da coluna da esquerda for par e somando os valores restantes da coluna da direita
			\begin{center}\begin{tabular}{c|c}
					\hline
					43			& 51			\\ \hline
					21			& 102 			\\ \hline
					\xout{10}	& \xout{204}	\\ \hline
					5			& 408 			\\ \hline
					\xout{2}	& \xout{816}	\\ \hline
					1			& 1632			\\ \hline
			\end{tabular}\end{center}
			
			\noindent A soma dos números da segunda coluna é o produto de 43 por 51, $$43\cdot 51 = 51 + 102 + 408 + 1632 = 2193.$$
		\end{exemplo}
		
		\begin{exemplo}
			Seguindo o algoritmo russo para resolver a multiplicação $72\cdot13$
			\begin{center}\begin{tabular}{c|c}
					\hline 
					\xout{72}	& \xout{13}		\\ \hline
					\xout{36}	& \xout{26}		\\ \hline
					\xout{18}	& \xout{52}		\\ \hline
					9			& 104			\\ \hline
					\xout{4}	& \xout{208}	\\ \hline
					\xout{2}	& \xout{416}	\\ \hline
					1			& 832			\\ \hline
			\end{tabular}\end{center}
			Portanto, o resultado é $72\cdot 13 = 104 + 832 = 936$.
		\end{exemplo}
		
		\subsubsection{Explicando o Algoritmo Russo}
			Considere dois números inteiros positivos $a$ e $b$. Pelo Algoritmo de Euclides \ref{algo_euclides}, podemos escrever o número $a$ como $a=2\cdot q_0+r_0$. Efetuando a multiplicação $a \cdot b$ pelo método russo, devemos construir a tabela abaixo
			\begin{center}\begin{tabular}{c|c}
				\hline
				$a=2\cdot q_0 + r_0$     				& $b$				\\ \hline
				$q_0 = 2\cdot q_1 + r_1$ 				& $2^1\cdot b$		\\ \hline
				$q_1 = 2\cdot q_2 + r_2$ 				& $2^2\cdot b$		\\ \hline
				$q_2 = 2\cdot q_3 + r_3$ 				& $2^3\cdot b$		\\ \hline
				$\vdots$                 				& $\vdots$			\\ \hline
				$q_{n-2} = 2\cdot q_{n-1} + r_{n-1}$	& $2^{n-1}\cdot b$	\\ \hline
				$q_{n-1} = 2\cdot q_n + r_n$         	& $2^n\cdot b$		\\ \hline
			\end{tabular}\end{center}
			
			\noindent sendo que o processo de duplicações e divisões por dois é finalizado quando obtemos $q_{n-1}=1$, consequentemente, $q_n = 0$ e $r_n = 1$. Note que utilizamos o algoritmo de Euclides na primeira coluna da tabela acima (veja a seção \ref{algo_euclides}) e a partir deste resultado, podemos encontrar a seguinte expressão para o fator $a$:
			\begin{equation}
				a=2^n + 2^{n-1}\cdot r_{n-1} + \cdots + 2^2\cdot r_2 + 2\cdot r_1 + r_0 \label{exp_russo1}
			\end{equation}			

			De fato, sabemos inicialmente que $a=2\cdot q_0 + r_0$, mas $q_0 = 2\cdot q_1 + r_1$ pela segunda linha da tabela. Substituindo a segunda equação na primeira, obtemos que $a=2^2\cdot q_1 + 2r_1 + r_0$ e repetindo este processo de substituições na expressão de $a$ encontramos a expressão (\ref{exp_russo1}).
			
			Aplicando o método russo para multiplicação, devemos descartar as linhas cujo número da coluna da esquerda for par, ou seja, dentre os elementos da primeira coluna, selecionamos os ímpares e isto significa que devemos escolher os elementos cujo resto seja um. Sendo assim, dentre $n$ restos $r_0,r_1,\ldots,r_n$, selecionamos os restos $r_{i_1},r_{i_2},\ldots,r_{i_k}$, com $k\leq n$ e todos valendo um. Este fato nos mostra que a expressão (\ref{exp_russo1}) pode ser reescrita como
			\begin{equation} a=2^{i_1}+2^{i_2}+\cdots + 2^{i_k} \label{exp_russo2}\end{equation}
			Utilizando a expressão (\ref{exp_russo2}), encontramos o resultado da multiplicação $a\cdot b$ somando os números da coluna da direita correspondentes aos elementos ímpares citados anteriormente
			$$2^{i_1}\cdot b+2^{i_2}\cdot b+\cdots+2^{i_k}\cdot b=(2^{i_1}+2^{i_2}+\cdots+2^{i_k})\cdot b=a\cdot b.$$
